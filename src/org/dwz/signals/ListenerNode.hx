package org.dwz.signals;

/**
 * ...
 * @author Dairectx
 */

class ListenerNode 
{
	public var previous:ListenerNode;
	public var next:ListenerNode;
	public var listener:Dynamic;
	public var once:Bool;

	public function new() 
	{
		
	}
	
}