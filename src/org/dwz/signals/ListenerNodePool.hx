package org.dwz.signals;

/**
 * ...
 * @author Dairectx
 */

class ListenerNodePool 
{
	private var tail:ListenerNode;
	private var cacheTail:ListenerNode;

	public function new() 
	{
		
	}
	
	public function get():ListenerNode
	{
		if ( tail != null )
		{
			var node:ListenerNode = tail;
			tail = tail.previous;
			node.previous = null;
			return node;
		}
		else
		{
			return new ListenerNode();
		}
	}
	
	public function dispose( node:ListenerNode ):Void
	{
		node.listener = null;
		node.once = false;
		node.next = null;
		node.previous = null;
		tail = node;
	}
	
	public function cache( node:ListenerNode ):Void
	{
		node.listener = null;
		node.previous = cacheTail;
		cacheTail = node;
	}
	
	public function releaseCache():Void
	{
		while ( cacheTail != null )
		{
			var node:ListenerNode = cacheTail;
			cacheTail = node.previous;
			node.next = null;
			node.previous = tail;
			tail = node;
		}
	}
	
}