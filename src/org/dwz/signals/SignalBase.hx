package org.dwz.signals;

/**
 * ...
 * @author Dairectx
 */
import nme.ObjectHash;
class SignalBase 
{
	public var head:ListenerNode;
	public var tail:ListenerNode;
	
	private var nodes:ObjectHash<Dynamic,ListenerNode>;
	private var listenerNodePool:ListenerNodePool;
	private var toAddHead:ListenerNode;
	private var toAddTail:ListenerNode;
	
	private var dispatching:Bool;
	
	public function new() 
	{
		nodes = new ObjectHash<Dynamic,ListenerNode>();
		listenerNodePool = new ListenerNodePool();
	}
	
	private function startDispatch():Void
	{
		dispatching = true;
	}
	
	private function endDispatch():Void
	{
		dispatching = false;
		if( toAddHead != null )
		{
			if( head == null )
			{
				head = toAddHead;
				tail = toAddTail;
			}
			else
			{
				tail.next = toAddHead;
				toAddHead.previous = tail;
				tail = toAddTail;
			}
			toAddHead = null;
			toAddTail = null;
		}
		listenerNodePool.releaseCache();
	}
	
	public function add(listener:Dynamic):Void
	{
		if ( nodes.exists( listener ) )
		{
			return;
		}
		var node:ListenerNode = listenerNodePool.get();
		node.listener = listener;
		nodes.set(listener, node );
		addNode( node );
	}
	
	public function addOnce( listener:Dynamic ):Void
	{
		if ( nodes.exists( listener ) )
		{
			return;
		}
		var node:ListenerNode = listenerNodePool.get();
		node.listener = listener;
		node.once = true;
		nodes.set( listener , node );
		addNode( node );
	}
	
	private function addNode( node:ListenerNode ):Void
	{
		if ( dispatching )
		{
			if ( toAddHead == null )
			{
				toAddHead = toAddTail = node;
			}
			else
			{
				toAddTail.next = node;
				node.previous = toAddTail;
				toAddTail = node;
			}
		}
		else
		{
			if ( head == null )
			{
				head = tail = node;
			}
			else
			{
				tail.next = node;
				node.previous = tail;
				tail = node;
			}
		}
	}
	
	public function remove( listener:Dynamic ):Void
	{
		var node:ListenerNode = nodes.get( listener );
		if ( node != null )
		{
			if ( head == node )
			{
				head = head.next;
			}
			if ( tail == node )
			{
				tail = tail.previous;
			}
			if ( toAddHead == node )
			{
				toAddHead = toAddHead.next;
			}
			if ( toAddTail == node )
			{
				toAddTail = toAddTail.previous;
			}
			if ( node.previous != null )
			{
				node.previous.next = node.next;
			}
			if ( node.next != null )
			{
				node.next.previous = node.previous;
			}
			nodes.remove(listener);
			if ( dispatching )
			{
				listenerNodePool.cache( node );
			}
			else
			{
				listenerNodePool.dispose( node );
			}
		}
	}
	
	public function removeAll():Void
	{
		while ( head != null )
		{
			var listener:ListenerNode = head;
			head = head.next;
			listener.previous = null;
			listener.next = null;
		}
		tail = null;
		toAddHead = null;
		toAddTail = null;
	}
}