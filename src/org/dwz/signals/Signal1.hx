package org.dwz.signals;

/**
 * ...
 * @author Dairectx
 */

class Signal1 extends SignalBase
{
	private var type:Class<Dynamic> ;
	public function new( type:Class<Dynamic> ) 
	{
		super();
		this.type = type;
	}
	
	public function dispatch( object:Dynamic ):Void
	{
		startDispatch();
		var node:ListenerNode;
		node = head;
		while ( node != null )
		{
			node.listener( object );
			if ( node.once )
			{
				remove( node.listener );
			}
			node = node.next;
		}
		endDispatch();
	}
	
}