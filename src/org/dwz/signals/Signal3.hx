package org.dwz.signals;

/**
 * ...
 * @author Dairectx
 */

class Signal3 
{

	private var type1:Class<Dynamic> ;
	private var type2:Class<Dynamic> ;
	private var type3:Class<Dynamic> ;
	public function new(typ1:Class<Dynamic> ,type2:Class<Dynamic> ,type3:Class<Dynamic> ) 
	{
		super();
		this.type1 = type1;
		this.type2 = type2;
		this.type3 = type3;
	}
	
	public function dispatch( object1:Dynamic , object2:Dynamic , object3:Dynamic ):Void
	{
		startDispatch();
		var node:ListenerNode;
		node = head;
		while ( node != null )
		{
			node.listener( object1, object2 , object3 );
			if ( node.once )
			{
				remove( node.listener );
			}
			node = node.next;
		}
		endDispatch();
	}
	
}