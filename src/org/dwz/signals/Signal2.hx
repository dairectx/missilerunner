package org.dwz.signals;

/**
 * ...
 * @author Dairectx
 */

class Signal2 extends SignalBase 
{
	private var type1:Class<Dynamic> ;
	private var type2:Class<Dynamic> ;
	public function new(type1:Class<Dynamic> ,type2:Class<Dynamic> ) 
	{
		super();
		this.type1 = type1;
		this.type2 = type2;
	}
	
	public function dispatch( object1:Dynamic , object2:Dynamic ):Void
	{
		startDispatch();
		var node:ListenerNode;
		node = head;
		while ( node != null )
		{
			node.listener( object1, object2 );
			if ( node.once )
			{
				remove( node.listener );
			}
			node = node.next;
		}
		endDispatch();
	}
	
}