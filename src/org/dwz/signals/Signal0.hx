package org.dwz.signals;

/**
 * ...
 * @author Dairectx
 */

class Signal0 extends SignalBase
{

	public function new() 
	{
		super();
	}
	
	public function dispatch():Void
	{
		startDispatch();
		var node:ListenerNode;
		node = head;
		while ( node != null )
		{
			node.listener();
			if ( node.once )
			{
				remove( node.listener );
			}
			node = node.next;
		}
		endDispatch();
	}
}