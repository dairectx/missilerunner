package org.dwz.signals;

/**
 * ...
 * @author Dairectx
 */

class SignalAny extends SignalBase 
{

	private var classes:Array<Dynamic>;
	public function new(classes:Array<Dynamic>) 
	{
		super();
		this.classes = classes;
	}
	
	public function dispatch( objects:Array<Dynamic> ):Void
	{
		startDispatch();
		var node:ListenerNode;
		node = head;
		while ( node != null )
		{
			Reflect.callMethod(null, node.listener, objects );
			if ( node.once )
			{
				remove( node.listener );
			}
			node = node.next;
		}
		endDispatch();
	}
	
}