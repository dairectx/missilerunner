package org.dwz.ashx.core;
import org.dwz.signals.Signal1;

/**
 * ...
 * @author Dairectx
 */

class NodeList 
{
	public var head:Dynamic;
	public var tail:Dynamic;
	public var nodeAdded:Signal1;
	public var nodeRemoved:Signal1;
	
	public function new() 
	{
		nodeAdded = new Signal1( Node );
		nodeRemoved = new Signal1( Node );
	}
	public function add( node:Node ):Void
	{
		if ( head == null )
		{
			head = tail = node;
		}
		else
		{
			tail.next = node;
			node.previous = tail;
			tail = node;
		}
		nodeAdded.dispatch( node );
	}
	
	public function remove( node:Node ):Void
	{
		if ( head == node )
		{
			head = head.next;
		}
		if ( tail == node )
		{
			tail = tail.previous;
		}
		if ( node.previous != null )
		{
			node.previous.next = node.next;
		}
		if ( node.next != null )
		{
			node.next.previous = node.previous;
		}
		nodeRemoved.dispatch( node );
		
	}
	
	public function removeAll():Void
	{
			while( head != null)
			{
				var node : Node = head;
				head = node.next;
				node.previous = null;
				node.next = null;
				nodeRemoved.dispatch( node );
			}
			tail = null;
	}
	
	public function swap( node1:Node , node2:Node ):Void
	{
			if( node1.previous == node2 )
			{
				node1.previous = node2.previous;
				node2.previous = node1;
				node2.next = node1.next;
				node1.next  = node2;
			}
			else if( node2.previous == node1 )
			{
				node2.previous = node1.previous;
				node1.previous = node2;
				node1.next = node2.next;
				node2.next  = node1;
			}
			else
			{
				var temp : Node = node1.previous;
				node1.previous = node2.previous;
				node2.previous = temp;
				temp = node1.next;
				node1.next = node2.next;
				node2.next = temp;
			}
			if( head == node1 )
			{
				head = node2;
			}
			else if( head == node2 )
			{
				head = node1;
			}
			if( tail == node1 )
			{
				tail = node2;
			}
			else if( tail == node2 )
			{
				tail = node1;
			}
			if( node1.previous != null )
			{							
				node1.previous.next = node1;
			}
			if( node2.previous != null )
			{
				node2.previous.next = node2;
			}
			if( node1.next != null)
			{
				node1.next.previous = node1;
			}
			if( node2.next != null )
			{
				node2.next.previous = node2;
			}
	}
	
	public function isEmpty():Bool
	{
		return head == null;
	}
}