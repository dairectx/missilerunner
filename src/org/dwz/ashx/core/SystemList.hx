package org.dwz.ashx.core;

/**
 * ...
 * @author Dairectx
 */

class SystemList 
{

	public var head:System;
	public var tail:System;
	
	public function add( system:System ):Void
	{
			if( head == null )
			{
				head = tail = system;
			}
			else
			{
				var node:System = tail;
				while ( node != null )
				{
					if( node.priority <= system.priority )
					{
						break;
					}
					node = node.previous;
				}
				if( node == tail )
				{
					tail.next = system;
					system.previous = tail;
					tail = system;
				}
				else if( node == null )
				{
					system.next = head;
					head.previous = system;
					head = system;
				}
				else
				{
					system.next = node.next;
					system.previous = node;
					node.next.previous = system;
					node.next = system;
				}
			}
	}
	
	public function remove( system:System ):Void
	{
			if ( head == system)
			{
				head = head.next;
			}
			if ( tail == system)
			{
				tail = tail.previous;
			}
			
			if (system.previous != null)
			{
				system.previous.next = system.next;
			}
			
			if (system.next != null)
			{
				system.next.previous = system.previous;
			}
	}
	
	public function removeAll():Void
	{
			while( head != null )
			{
				var system : System = head;
				head = head.next;
				system.previous = null;
				system.next = null;
			}
			tail = null;
	}
	
	public function get( type:Class<Dynamic> ):System
	{
		var system:System = head;
		while ( system != null )
		{
			if ( Std.is( system , type ) )
			{
				return system;
			}
			system = system.next;
		}
		return null;
	}
	public function new() 
	{
		
	}
	
}