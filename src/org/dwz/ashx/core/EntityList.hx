package org.dwz.ashx.core;

/**
 * ...
 * @author Dairectx
 */

class EntityList 
{
	public var head:Entity;
	public var tail:Entity;
	public function new() 
	{
		
	}
	public function add(entity:Entity):Void
	{
		if ( head == null )
		{
			head = tail = entity;
		}
		else
		{
			tail.next = entity;
			entity.previous = tail;
			tail = entity;
		}
	}
	
	public function remove( entity:Entity ):Void
	{
		if ( head == entity )
		{
			head = head.next;
		}
		if ( tail == entity )
		{
			tail = tail.previous;
		}
		if (entity.previous != null)
		{
			entity.previous.next = entity.next;
		}
		if ( entity.next != null )
		{
			entity.next.previous = entity.previous;
		}
	}
	
	public function removeAll():Void
	{
		while( head != null )
		{
			var entity : Entity = head;
			head = head.next;
			entity.previous = null;
			entity.next = null;
		}
		tail = null;
	}
}