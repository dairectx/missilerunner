package org.dwz.ashx.core;

/**
 * ...
 * @author Dairectx
 */
import org.dwz.signals.Signal0;
import nme.ObjectHash;
class Game 
{
	private var entities:EntityList;
	private var systems:SystemList;
	private var families:ObjectHash<Class<Dynamic>,Family>;
	
	public var updating:Bool;
	
	public var updateComplete:Signal0;
	
	public function new() 
	{
		entities = new EntityList();
		systems = new SystemList();
		families = new ObjectHash<Class<Dynamic>,Family>();
		updateComplete = new Signal0();
	}
	
	public function addEntity( entity:Entity ):Void
	{
		entities.add( entity );
		entity.componentAdded.add( componentAdded );
		
		for ( family in families )
		{
			family.addIfMatch( entity );
		}

	}
	public function removeEntity( entity:Entity ):Void
	{
		
		for ( family in families )
		{
			family.remove( entity );
		}
		entity.componentAdded.remove(componentAdded);
		entities.remove(entity);
	}
	
	public function removeAllEntities():Void
	{
		while ( entities.head != null )
		{
			removeEntity( entities.head );
		}
	}
	
	private function componentAdded( entity:Entity , componentClass:Class<Dynamic> ):Void
	{		
		for ( family in families )
		{
			family.addIfMatch( entity );
		}
	}
	
	public function getNodeList( nodeClass:Class<Dynamic> ):NodeList
	{
		if ( families.exists( nodeClass ) )
		{
			
			return families.get(nodeClass).nodes; 
		}
		var family:Family = new Family( nodeClass , this);
		families.set(nodeClass, family);
		//todo:更好方法
		var entity:Entity = entities.head;
		while ( entity != null )
		{
			family.addIfMatch( entity );
			entity = entity.next;
		}
		return family.nodes;
	}
	
	public function releaseNodeList( nodeClass:Class<Dynamic>):Void
	{
		if ( families.exists(nodeClass) )
		{
			families.get(nodeClass).cleanUp();
		}
		families.remove(nodeClass);
	}
	
	public function addSystem( system:System , priority:Int ):Void
	{
		system.priority = priority;
		system.addToGame(this);
		systems.add(system);
	}
	
	public function getSystem( type:Class<Dynamic> ):System
	{
		return systems.get( type );
	}
	
	public function removeAllSystems():Void
	{
		while ( systems.head != null )
		{
			removeSystem( systems.head );
		}
	}
	
	public function removeSystem( system:System ):Void
	{
		systems.remove(system);
		system.removeFromGame(this);
	}
	public function update( time:Float ):Void
	{
		updating = true;
		var system:System = systems.head;
		//todo:更快的速度
		while (system != null)
		{
			system.update(time);
			system = system.next;
		}
		updating = false;
		updateComplete.dispatch();
	}
}