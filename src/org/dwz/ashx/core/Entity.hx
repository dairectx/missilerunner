package org.dwz.ashx.core;
/**
 * ...
 * @author Dairectx
 */
import org.dwz.signals.Signal2;
import nme.ObjectHash;
class Entity 
{
	public var name:String;
	
	public var componentAdded:Signal2;
	public var componentRemoved:Signal2;
	public var previous:Entity;
	public var next:Entity;
	public var components:ObjectHash<Class<Dynamic>,Dynamic>;
	
	public function new() 
	{
		componentAdded = new Signal2( Entity , Class  );
		componentRemoved = new Signal2( Entity , Class );
		components = new ObjectHash<Class<Dynamic>,Dynamic>();
	}
	
	public function add( component:Dynamic , ?componentClass:Class<Dynamic> ):Entity
	{
		if ( componentClass == null )
		{
			componentClass = Type.getClass(component);
		}
		if ( components.exists(componentClass) )
		{
			remove( componentClass );
		}
		components.set(componentClass, component );
		componentAdded.dispatch( this, componentClass );
		return this;
	}
	
	public function remove( componentClass:Class<Dynamic> ):Dynamic
	{
		var component:Dynamic = components.get( componentClass );
		if ( component )
		{
			components.remove( componentClass );
			componentRemoved.dispatch( this, componentClass );
			return component;
		}
		return null;
	}
	
	public function get( componentClass:Class<Dynamic>):Dynamic
	{
		return components.get(componentClass );
	}
	
	public function has( componentClass:Class<Dynamic> ):Bool
	{
		return components.get( componentClass ) != null;
	}
	//issue:可能会有错误，暂不使用
	public function clone():Entity
	{
		return deepCopy(this);
	}
	
	private function deepCopy<T>( v:T ) : T
	{
		if (!Reflect.isObject(v))
		{
			return v;
		}
		else if( Std.is( v, Array ) ) 
		{
			var r = Type.createInstance(Type.getClass(v), []);
			untyped
			{
				for( ii in 0...v.length )
				r.push(deepCopy(v[ii]));
			}
			return r;
		}
		else if( Type.getClass(v) == null )
		{
			var obj : Dynamic = {};
			for( ff in Reflect.fields(v) )
			Reflect.setField(obj, ff, deepCopy(Reflect.field(v, ff)));
			return obj;
		}
		else
		{
			var obj = Type.createEmptyInstance(Type.getClass(v));
			for( ff in Reflect.fields(v) )
				Reflect.setField(obj, ff, deepCopy(Reflect.field(v, ff)));
			return obj;
		}
		return null;
  } 
	
	
}