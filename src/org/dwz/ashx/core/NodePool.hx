package org.dwz.ashx.core;

/**
 * ...
 * @author Dairectx
 */

class NodePool 
{
	private var tail:Node;
	private var nodeClass:Class<Dynamic>;
	private var cacheTail:Node;
	
	public function new( nodeClass:Class<Dynamic> ) 
	{
		this.nodeClass = nodeClass;
	}
	
	public function get():Node
	{
		if ( tail != null )
		{
			var node:Node = tail;
			tail = tail.previous;
			node.previous = null;
			return node;
		}
		else
		{
			return Type.createEmptyInstance( nodeClass );
		}
	}
	
	public function dispose( node:Node ):Void
	{
		node.next = null;
		node.previous = tail;
		tail = node;
	}
	
	public function cache( node:Node ):Void
	{
		node.previous = cacheTail;
		cacheTail = node;
	}
	
	public function releaseCache():Void
	{
		while ( cacheTail != null )
		{
			var node:Node = cacheTail;
			cacheTail = node.previous;
			node.next = null;
			node.previous = tail;
			tail = node;
		}
	}
	
}