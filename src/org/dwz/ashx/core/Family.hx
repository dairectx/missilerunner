package org.dwz.ashx.core;

/**
 * ...
 * @author Dairectx
 */
import haxe.rtti.CType;
import nme.ObjectHash;
class Family 
{

	public var previous:Family;
	public var next:Family;
	public var nodes:NodeList;
	public var entities:ObjectHash<Entity,Node>;
	
	private var nodeClass:Class<Dynamic>;
	private var components:ObjectHash < Class<Dynamic>, String > ;
	private var nodePool:NodePool;
	private var game:Game;
	public function new( nodeClass:Class<Dynamic> , game:Game)
	{
		this.nodeClass = nodeClass;
		this.game = game;
		init();
	}
	
	private function init():Void
	{
		//使用rtti方法实现，node继承了一个rtti的info接口。性能代价有多少？todo:macros?
		nodePool = new NodePool(  nodeClass );
		nodes = new NodeList();
		entities = new ObjectHash<Entity,Node>();
		
		components = new ObjectHash < Class<Dynamic>, String >();
		nodePool.dispose( nodePool.get() );
		
		var node:Class<Dynamic> = Type.getClass(Type.createEmptyInstance( nodeClass ));
		var rstr:String = Reflect.field( node , "__rtti" );
		var x = Xml.parse(rstr).firstElement();
		var infos = new haxe.rtti.XmlParser().processElement(x);
		var c:Classdef;
		c = null;
		switch(infos)
		{
        case TClassdecl(c):
            for (f in c.fields)
            {
                if (f.name != "entity" &&f.name != "previous" &&f.name != "next")
                {
                    var type:String = Std.string(f.type);
					var arr:Array<String> = type.split("(");
					arr.shift();
					type = arr.pop();
					arr = type.split(",");
					arr.pop();
					var classname:String = arr.pop();
					if ( classname == "{}" || classname == "Void"  )
						continue;
					var newClass:Class<Dynamic> = Type.resolveClass(classname);
					
					components.set(newClass, f.name );
                }
            }
        default:
		}
	}
	public function addIfMatch( entity:Entity ):Void
	{
		if ( entities.get( entity ) == null )
		{
			var keys:Iterator<Class<Dynamic>> = components.keys();
			for ( componentClass in keys )
			{
				if ( !entity.has( componentClass ) )
				{
					return;
				}
			}
			var node:Node = nodePool.get();
			node.entity = entity;		
						
			
			keys = components.keys();
			for ( componentClass in keys )
			{
				var fieldName:String = components.get(componentClass);
				Reflect.setField(node, fieldName, entity.get( componentClass  ));
				
			}
			entities.set(entity, node);
			entity.componentRemoved.add( componentRemoved );
			nodes.add( node );
		}
	}
	public function remove(entity:Entity):Void
	{
		if( entities.exists(entity))
		{
			var node:Node = entities.get( entity );
			entity.componentRemoved.remove( componentRemoved );
			entities.remove(entity);
			nodes.remove( node);
			if ( game.updating )
			{
				nodePool.cache( node );
				game.updateComplete.add( releaseNodePoolCache );
			}
			else
			{
				nodePool.dispose( node );
			}
		}
	}
	
	private function releaseNodePoolCache():Void
	{
		game.updateComplete.remove( releaseNodePoolCache );
		nodePool.releaseCache();
	}
	public function cleanUp():Void
	{
		var node:Node = nodes.head;
		while ( node != null )
		{
			node.entity.componentRemoved.remove( componentRemoved );
			entities.remove(node.entity);
			node = node.next;
		}
		nodes.removeAll();
	}
	private function componentRemoved( entity:Entity , componentClass:Class<Dynamic> ):Void
	{
		if ( components.exists(componentClass) )
		{
			remove( entity );
		}
	}
}