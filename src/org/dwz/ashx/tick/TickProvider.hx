package org.dwz.ashx.tick;

/**
 * ...
 * @author Dairectx
 */

interface TickProvider 
{
	function add( listener:Dynamic ):Void;
	function remove( listener:Dynamic ):Void;
	function start():Void;
	function stop():Void;
	
}