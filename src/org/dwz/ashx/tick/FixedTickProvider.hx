package org.dwz.ashx.tick;
import flash.display.DisplayObject;
import org.dwz.ashx.signals.Signal1;
import flash.events.Event;
/**
 * ...
 * @author Dairectx
 */

class FixedTickProvider extends Signal1 implements TickProvider
{
	private var displayObject:DisplayObject;
	private var frameTime:Float;
	
	public var timeAdjustment:Float;
	public function new( displayObject:DisplayObject , frameTime:Float) 
	{
		super( Float );
		this.displayObject = displayObject;
		this.frameTime = frameTime;
		timeAdjustment = 1;
	}
	
	public function start():Void
	{
		displayObject.addEventListener( Event.ENTER_FRAME , dispatchTick );
	}
	
	public function stop():Void
	{
		displayObject.removeEventListener( Event.ENTER_FRAME , dispatchTick);
	}
	
	private function dispatchTick( event:Event ):Void
	{
		dispatch(frameTime * timeAdjustment);
	}
}