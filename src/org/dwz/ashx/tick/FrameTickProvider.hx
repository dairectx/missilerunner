package org.dwz.ashx.tick;
import flash.display.DisplayObject;
import flash.Lib;
import flash.events.Event;
import org.dwz.signals.Signal1;
/**
 * ...
 * @author Dairectx
 */

class FrameTickProvider extends Signal1, implements TickProvider
{
	private var displayObject:DisplayObject;
	private var previousTime:Float;
	private var maximumFrameTime:Float;
	
	public var timeAdjustment:Float;
	public function new( displayObject:DisplayObject , ?maximumFrameTime:Float = 99999999) 
	{
		super( Float );
		this.displayObject = displayObject;
		this.maximumFrameTime = maximumFrameTime;
		timeAdjustment = 1;
	}
	
	public function start():Void
	{
		previousTime = Lib.getTimer();
		displayObject.addEventListener( Event.ENTER_FRAME , dispatchTick );
	}
	
	public function stop():Void
	{
		displayObject.removeEventListener( Event.ENTER_FRAME , dispatchTick );
	}
	
	private function dispatchTick( event:Event ):Void
	{
		var temp:Float = previousTime;
		previousTime = Lib.getTimer();
		var frameTime:Float = ( previousTime - temp ) / 1000;
		if ( frameTime > maximumFrameTime )
		{
			frameTime = maximumFrameTime;
		}
		dispatch( frameTime * timeAdjustment );
	}
}