package org.dwz.game.missilerrunner.components;

/**
 * ...
 * @author Dairectx
 */

class GameState 
{

	public var level:Int;
	public var lives:Int;
	public var width:Float;
	public var height:Float;
	public var scale:Float;
	public var state:String;
	public function new() 
	{
		width = 0;
		height = 0;
		state = "playing";
	}
	
}