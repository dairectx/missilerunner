package org.dwz.game.missilerrunner.components;
import nme.geom.Point;

/**
 * ...
 * @author Dairectx
 */

class Motion 
{

	public var velocity:Point ;
	public var angularVelocity:Float;
	public var damping:Float;
	public function new() 
	{
		velocity = new Point(0,0);
		angularVelocity = 0;
		damping = 0;
	}
	
}