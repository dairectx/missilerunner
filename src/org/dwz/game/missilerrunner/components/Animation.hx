package org.dwz.game.missilerrunner.components;

/**
 * ...
 * @author Dairectx
 */

class Animation 
{
	public var delays:Array<Int>;
	public var name:String;
	public var offsetTileID:Int;
	public var totalFrameNum:Int;
	public var currentFrameNum:Int;
	public var lastUpdateTime:Float;
	public var currentTime:Float;
	public function new() 
	{
		currentTime = 0;
		lastUpdateTime = 0;
	}
	
}