package org.dwz.game.missilerrunner.components;

/**
 * ...
 * @author Dairectx
 */

class MotionControls 
{

	public var accelerate_x:Float;//加速计水平分量
	public var accelerate_y:Float;//加速计竖直分量
	public var accelerate_x_base:Float;//基准量
	public var accelerate_y_base:Float;
	public function new() 
	{
		accelerate_x = 0;
		accelerate_y = 0;
		
	}
	
}