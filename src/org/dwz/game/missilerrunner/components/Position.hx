package org.dwz.game.missilerrunner.components;
import nme.geom.Point;

/**
 * ...
 * @author Dairectx
 */

class Position 
{

	public var position:Point;
	public var rotation:Float;
	public var collisionRadius:Float;
	public function new() 
	{
		position = new Point();
		rotation = 0;
		collisionRadius = 0;
	}
	
}