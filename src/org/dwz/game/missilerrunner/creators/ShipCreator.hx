package org.dwz.game.missilerrunner.creators;
import nme.geom.Point;
import nme.ui.Acceleration;
import nme.ui.Accelerometer;
import org.dwz.ashx.core.Entity;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.core.NodeList;
import org.dwz.game.missilerrunner.components.Animation;
import org.dwz.game.missilerrunner.components.CloseShot;
import org.dwz.game.missilerrunner.components.Display;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.MotionControls;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.components.Ship;
import org.dwz.game.missilerrunner.components.TileSheet;
/**
 * ...
 * @author Dairectx
 */

class ShipCreator implements ICreator
{

	private var _game:Game;
	private var _nodes:NodeList;
	private var _tileSheet:TileSheet;
	public function new( game:Game , tileSheet:TileSheet ) 
	{
		_game = game;
		_tileSheet = tileSheet;
	}
	
	public function destory(entity:Entity):Void
	{
		
	}
	public function create(params:Array<Dynamic>):Entity
	{
		var entity:Entity = new Entity();
		
		//飞船
		var ship:Ship = new Ship();
		entity.add( ship );
		
		//图像
		var display:Display = new Display();
		entity.add( display );
		
		//动作
		var motion:Motion = new Motion();
		motion.damping = 0.2;		
		entity.add( motion );
		
		//动作控制
		var motionControls:MotionControls = new MotionControls();
#if android		
		var acceleration:Acceleration = Accelerometer.get();//init
		motionControls.accelerate_x_base = acceleration.x;
		motionControls.accelerate_y_base = acceleration.y;
#end
		entity.add( motionControls );
		
		//位置
		var position:Position = new Position();
		position.collisionRadius = 20;
		position.position = new Point( Math.random()*400, Math.random()*400);
		position.rotation = 0;
		entity.add( position );
		
		//位于近景
		var layer:CloseShot = new CloseShot();
		entity.add( layer );
		
		//动画
		var animation:Animation = new Animation();
		animation.name  = "dog";
		animation.offsetTileID = _tileSheet.nameToOffsetTileID.get("dog");
		animation.currentFrameNum = 0;
		animation.totalFrameNum = 10;
		animation.delays = [ 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 ];
		entity.add(animation);
		
		_game.addEntity( entity );
		
		return entity;
		
	}
	
}