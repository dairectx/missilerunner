package org.dwz.game.missilerrunner.creators;
import org.dwz.ashx.core.Entity;

/**
 * ...
 * @author Dairectx
 */

interface ICreator 
{
	function destory(entity:Entity):Void;
	function create(params:Array<Dynamic>):Entity;
	
}