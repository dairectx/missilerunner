package org.dwz.game.missilerrunner.creators;
import nme.geom.Point;
import org.dwz.ashx.core.Entity;
import org.dwz.ashx.core.Game;
import org.dwz.game.missilerrunner.components.Animation;
import org.dwz.game.missilerrunner.components.Asteroid;
import org.dwz.game.missilerrunner.components.Display;
import org.dwz.game.missilerrunner.components.GameState;
import org.dwz.game.missilerrunner.components.MidShot;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.MotionControls;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.components.Rocket;
import org.dwz.game.missilerrunner.components.TileSheet;
/**
 * ...
 * @author Dairectx
 */

class AsteroidCreator  implements ICreator
{
	private var _game:Game;
	private var _gameState:GameState;
	private var _tileSheet:TileSheet;
	
	public function new( game:Game , gameState:GameState , tileSheet:TileSheet ) 
	{
		_game  = game;
		_gameState = gameState;
		_tileSheet = tileSheet;
	}
	
	public function destory(entity:Entity):Void
	{
		
	}
	
	public function create(params:Array<Dynamic>):Entity
	{
		var entity:Entity = new Entity();
		
		//飞船
		var asteroid:Asteroid = new Asteroid();
		entity.add( asteroid );
		
		//图像
		var display:Display = new Display();
		entity.add( display );
		
		//动作
		var motion:Motion = new Motion();
		motion.damping = 0.2;	
		motion.velocity.y  = 50;
		entity.add( motion );
		
		//动作控制
		var motionControls:MotionControls = new MotionControls();
		entity.add( motionControls );
		
		//位置
		var position:Position = new Position();
		position.collisionRadius = 20;
		position.position = new Point( Math.random()*_gameState.width, 0-Math.random()*900);//从屏幕顶部0-900距离随机生成
		position.rotation = 0;
		entity.add( position );
		
		//中景
		var layer:MidShot = new MidShot();
		entity.add( layer );
		
		var animation:Animation = new Animation();
		animation.name  = "meteor" + Std.string(Std.int(Math.random()*3));
		animation.offsetTileID = _tileSheet.nameToOffsetTileID.get(animation.name);
		animation.currentFrameNum = 0;
		animation.totalFrameNum = 1;
		animation.delays = [ 10000 ];
		entity.add(animation);
		
		_game.addEntity( entity );
		
		return entity;
	}
	
}