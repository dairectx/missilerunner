package org.dwz.game.missilerrunner;

import net.hires.debug.Stats;
import nme.display.Sprite;
import nme.display.StageAlign;
import nme.display.StageScaleMode;
import nme.events.Event;
import nme.Lib;

/**
 * ...
 * @author Dairectx
 */

class Main extends Sprite
{
	private var missilerRunner:MissilerRunner;
	static public function main() 
	{
		var stage = Lib.current.stage;
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;
		// entry point
		Lib.current.addChild( new Main() );
	}
	public function new() 
	{
		super();
		this.addEventListener(Event.ADDED_TO_STAGE , init );
	}
	private function init( event:Event ):Void
	{
		this.removeEventListener( Event.ADDED_TO_STAGE, init );
		var scale:Float;
		//scale = stage.stageWidth / 800 < stage.stageHeight / 1200 ? stage.stageWidth / 800 : stage.stageHeight / 1200 ;
		scale = 1;
		Lib.current.scaleX = scale;
		Lib.current.scaleY = scale;
		missilerRunner = new MissilerRunner( this , stage.stageWidth , stage.stageHeight );
		missilerRunner.start();
		var ststs:Stats = new Stats();
		Lib.current.addChild( ststs );
	}
	
}