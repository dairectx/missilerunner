package org.dwz.game.missilerrunner.nodes.motion;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Asteroid;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.MotionControls;
import org.dwz.game.missilerrunner.components.Position;

/**
 * ...
 * @author Dairectx
 */

class AsteroidsControlNode extends Node
{

	public var motioncontrols:MotionControls;
	public var motion:Motion;
	public var asteroid:Asteroid;
	public function new() 
	{
		super();		
	}
	
}