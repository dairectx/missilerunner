package org.dwz.game.missilerrunner.nodes.motion;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Monster;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.MotionControls;

/**
 * ...
 * @author Dairectx
 */

class MonsterControlNode extends Node
{
	public var motioncontrols:MotionControls;
	public var motion:Motion;
	public var monster:Monster;
	public function new() 
	{
		super();		
	}
	
}