package org.dwz.game.missilerrunner.nodes.motion;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.MotionControls;
import org.dwz.game.missilerrunner.components.Rocket;

/**
 * ...
 * @author Dairectx
 */

class RocketsControlNode extends Node
{
	public var control:MotionControls;
	public var motion:Motion;
	public var rocket:Rocket;
	public function new() 
	{
		super();		
	}
	
}