package org.dwz.game.missilerrunner.nodes.motion;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.MotionControls;
import org.dwz.game.missilerrunner.components.Ship;

/**
 * ...
 * @author Dairectx
 */

class ShipControlNode extends Node
{
	public var control:MotionControls;
	public var motion:Motion;
	public var ship:Ship;
	public function new() 
	{
		super();		
	}
	
}