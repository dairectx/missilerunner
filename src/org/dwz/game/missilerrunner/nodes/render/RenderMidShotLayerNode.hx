package org.dwz.game.missilerrunner.nodes.render;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Animation;
import org.dwz.game.missilerrunner.components.CloseShot;
import org.dwz.game.missilerrunner.components.Display;
import org.dwz.game.missilerrunner.components.MidShot;
import org.dwz.game.missilerrunner.components.Position;
/**
 * ...
 * @author Dairectx
 */

class RenderMidShotLayerNode extends Node
{
	public var display:Display;
	public var position:Position;
	public var layer:MidShot;
	public var animation:Animation;
	public function new() 
	{
		super();
	}
	
}