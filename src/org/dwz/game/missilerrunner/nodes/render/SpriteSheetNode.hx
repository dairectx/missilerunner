package org.dwz.game.missilerrunner.nodes.render;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.SpriteSheet;

/**
 * ...
 * @author Dairectx
 */

class SpriteSheetNode extends Node
{
	public var spriteSheet:SpriteSheet;
	public function new() 
	{
		super();
	}
	
}