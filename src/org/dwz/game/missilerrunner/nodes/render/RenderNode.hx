package org.dwz.game.missilerrunner.nodes.render;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Display;
import org.dwz.game.missilerrunner.components.Position;

/**
 * ...
 * @author Dairectx
 */

class RenderNode extends Node
{

	public var display:Display;
	public var position:Position;
	
	public function new() 
	{
		super();		
	}
	
}