package org.dwz.game.missilerrunner.nodes.movement;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.components.Ship;

/**
 * ...
 * @author Dairectx
 */

class ShipMovementNode extends Node
{

	public var ship:Ship;
	public var position:Position;
	public var motion:Motion;
	
	public function new() 
	{
		super();		
	}
	
}