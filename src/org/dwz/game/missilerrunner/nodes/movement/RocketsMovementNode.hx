package org.dwz.game.missilerrunner.nodes.movement;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.components.Rocket;

/**
 * ...
 * @author Dairectx
 */

class RocketsMovementNode extends Node
{
	public var rocket:Rocket;
	public var position:Position;
	public var motion:Motion;

	public function new() 
	{
		super();		
	}
	
}