package org.dwz.game.missilerrunner.nodes.movement;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Asteroid;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.Position;

/**
 * ...
 * @author Dairectx
 */

class AsteroidsMovementNode extends Node
{

	public var asteroid:Asteroid;
	public var position:Position;
	public var motion:Motion;
	public function new() 
	{
		super();		
	}
	
}