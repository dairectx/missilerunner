package org.dwz.game.missilerrunner.nodes.movement;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Monster;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.Position;

/**
 * ...
 * @author Dairectx
 */

class MonsterMovementNode extends Node
{
	public var monster:Monster;
	public var position:Position;
	public var motion:Motion;

	public function new() 
	{
		super();		
	}
	
}