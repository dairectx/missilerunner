package org.dwz.game.missilerrunner.nodes.collision;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.components.Ship;

/**
 * ...
 * @author Dairectx
 */

class ShipCollisionNode extends Node
{

	public var ship:Ship;
	public var position:Position;
	
	public function new() 
	{
		super();		
	}
	
}