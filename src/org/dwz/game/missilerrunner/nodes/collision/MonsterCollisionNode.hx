package org.dwz.game.missilerrunner.nodes.collision;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Monster;
import org.dwz.game.missilerrunner.components.Position;

/**
 * ...
 * @author Dairectx
 */

class MonsterCollisionNode extends Node
{

	public var monster:Monster;
	public var position:Position;
	public function new() 
	{
		super();
	}
	
}