package org.dwz.game.missilerrunner.nodes.collision;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.components.Rocket;

/**
 * ...
 * @author Dairectx
 */

class RocketCollisionNode extends Node
{

	public var rocket:Rocket;
	public var position:Position;
	
	public function new() 
	{
		super();		
	}
	
}