package org.dwz.game.missilerrunner.nodes.collision;
import org.dwz.ashx.core.Node;
import org.dwz.game.missilerrunner.components.Asteroid;
import org.dwz.game.missilerrunner.components.Position;

/**
 * ...
 * @author Dairectx
 */

class AsteroidsCollisionNode extends Node
{

	public var asteroid:Asteroid;
	public var position:Position;
	
	public function new() 
	{
		super();		
	}
	
}