package org.dwz.game.missilerrunner.systems.collision;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.core.NodeList;
import org.dwz.ashx.core.System;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.nodes.collision.AsteroidsCollisionNode;
import org.dwz.game.missilerrunner.nodes.collision.MonsterCollisionNode;
import org.dwz.game.missilerrunner.nodes.collision.RocketCollisionNode;
import org.dwz.game.missilerrunner.nodes.collision.ShipCollisionNode;

/**
 * ...
 * @author Dairectx
 */

class CollisionSystem extends System
{
	private var _shipNodes:NodeList;
	private var _rocketNodes:NodeList;
	private var _monsterNodes:NodeList;
	private var _asteroidNodes:NodeList;
	
	public function new() 
	{
		super();		
	}
	
	override public function addToGame(game:Game):Void 
	{
		super.addToGame(game);
		_shipNodes = game.getNodeList(ShipCollisionNode);
		_rocketNodes = game.getNodeList(RocketCollisionNode);
		_monsterNodes = game.getNodeList(MonsterCollisionNode);
		_asteroidNodes = game.getNodeList(AsteroidsCollisionNode);
	}
	
	override public function update(time:Float):Void 
	{
		super.update(time);
		var ship:ShipCollisionNode = _shipNodes.head;//只有一个飞船，所以就不需要遍历了，直接取第一个
		var shipPosition:Position = ship.position;
		if (ship == null) return;
		
		var asteroid:AsteroidsCollisionNode = _asteroidNodes.head;//先让飞船和小行星做碰撞检测
		while ( asteroid != null )
		{
			
			asteroid = asteroid.next;
		}
		
		var rocket:RocketCollisionNode = _rocketNodes.head;//其次是飞船和火箭做碰撞检测
		while ( rocket != null )
		{
			rocket = rocket.next;
		}
		
		var monster:MonsterCollisionNode = _monsterNodes.head;//然后是飞船和怪物的碰撞检测
		while ( monster != null )
		{
			
			monster = monster.next;
		}
		
	}
	
	private function isCollision():Bool
	{
		return true;
	}
	override public function removeFromGame(game:Game):Void 
	{
		super.removeFromGame(game);
		_shipNodes = null;
		_asteroidNodes = null;
		_monsterNodes  = null;
		_rocketNodes = null;
	}
	
}