package org.dwz.game.missilerrunner.systems.render;
import nme.display.DisplayObjectContainer;
import nme.display.Shape;
import nme.display.Tilesheet;
import nme.geom.Rectangle;
import nme.Lib;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.core.NodeList;
import org.dwz.ashx.core.System;
import org.dwz.game.missilerrunner.components.Animation;
import org.dwz.game.missilerrunner.components.Display;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.components.TileSheet;
import org.dwz.game.missilerrunner.nodes.render.RenderCloseShotLayerNode;
import org.dwz.game.missilerrunner.nodes.render.RenderNode;
import org.dwz.game.missilerrunner.nodes.render.RenderVisitorNode;
import org.dwz.game.missilerrunner.nodes.render.RenderMidShotLayerNode;
/**
 * ...
 * @author Dairectx
 */

class RenderSystem extends System
{
	private var _closeShotNodes:NodeList;
	private var _midShotNodes:NodeList;
	private var _container:DisplayObjectContainer;
	private var tileSheet:RenderVisitorNode;
	private var _tileSheet:Tilesheet;
	private var _shape:Shape;
	private var _isReverse:Bool;
	private var _step:Int;
	private var _scale:Float;
	public function new( container:DisplayObjectContainer) 
	{
		super();
		_container = container;
		_isReverse = false;
		_step = 1;
		
	}
	private function addToDisplay( node : RenderNode ) : Void
	{
		_container.addChild( node.display.displayObject );
	}
		
	private function removeFromDisplay( node : RenderNode ) : Void
	{
		_container.removeChild( node.display.displayObject );
	}
	
	override public function addToGame(game:Game):Void 
	{
		super.addToGame(game);
		_closeShotNodes = game.getNodeList( RenderCloseShotLayerNode );
		_midShotNodes = game.getNodeList( RenderMidShotLayerNode );
		
		tileSheet = game.getNodeList( RenderVisitorNode ).head;
		_tileSheet = tileSheet.tileSheet.tileSheet;
		
		_scale = Lib.current.stage.stageWidth / 960 < Lib.current.stage.stageHeight / 1440 ? Lib.current.stage.stageWidth / 960 : Lib.current.stage.stageHeight / 1440;
		
		_shape = tileSheet.tileSheet.canva;
		this._container.addChild(_shape );
		
	}
	
	override public function update(time:Float):Void 
	{
		super.update(time);
		///////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////
		var closeShotLayerNode:RenderCloseShotLayerNode;
		var display:Display;
		var position:Position;
		var animation:Animation;
		var drawData:Array<Float>;
		drawData = tileSheet.tileSheet.drawData;
		closeShotLayerNode = _closeShotNodes.head;
		var index:Int = 0;
		index = tileSheet.tileSheet.drawDataOffset;
		var delay:Int = 0;
		
		///////////////////////////////////////////////////////////////
		var midShotNodes:RenderMidShotLayerNode;
		midShotNodes = _midShotNodes.head;
		index = tileSheet.tileSheet.drawDataOffset;
		while ( midShotNodes != null )
		{
			position = midShotNodes.position;
			
			drawData[index] = position.position.x;//x
			drawData[index + 1] = position.position.y;//y
			
			animation  = midShotNodes.animation;
			delay = animation.delays[animation.currentFrameNum];
			animation.currentTime += time * 1000;
			if ( animation.currentTime - animation.lastUpdateTime  < delay  )
			{
				drawData[index + 2] = animation.offsetTileID + animation.currentFrameNum;
			}
			else
			{
				animation.lastUpdateTime = animation.currentTime;
				if ( _isReverse == false && animation.currentFrameNum + _step == animation.totalFrameNum )
				{
					_isReverse = true;
					_step = -1;
					
				}
				else if ( _isReverse == true && animation.currentFrameNum + _step == -1 )
				{
					_isReverse = false;
					_step = 1;
				}
				else
				{
					animation.currentFrameNum = (animation.currentFrameNum +_step) % animation.totalFrameNum;
				}
				drawData[index + 2] = animation.offsetTileID + animation.currentFrameNum ;//tileID
			}
			drawData[index +3] = _scale;
			index = index + 4;
			midShotNodes = midShotNodes.next;
		}
		////////////////////////////////////////////////////////
		
		while ( closeShotLayerNode != null )
		{
			position = closeShotLayerNode.position;
			
			drawData[index] = position.position.x;//x
			drawData[index + 1] = position.position.y;//y
			
			animation  = closeShotLayerNode.animation;
			delay = animation.delays[animation.currentFrameNum];
			animation.currentTime += time * 1000;
			if ( animation.currentTime - animation.lastUpdateTime  < delay  )
			{
				drawData[index + 2] = animation.offsetTileID + animation.currentFrameNum;
			}
			else
			{
				animation.lastUpdateTime = animation.currentTime;
				if ( _isReverse == false && animation.currentFrameNum + _step == animation.totalFrameNum )
				{
					_isReverse = true;
					_step = -1;
					
				}
				else if ( _isReverse == true && animation.currentFrameNum + _step == -1 )
				{
					_isReverse = false;
					_step = 1;
				}
				else
				{
					animation.currentFrameNum = (animation.currentFrameNum +_step) % animation.totalFrameNum;
				}
				drawData[index + 2] = animation.offsetTileID + animation.currentFrameNum ;//tileID
			}
			drawData[index +3] = _scale;
			index = index + 4;
			closeShotLayerNode = closeShotLayerNode.next;
		}
		tileSheet.tileSheet.drawDataOffset = index;
		
		////////////////////////////////////////////////////////
		
		_shape.graphics.clear();
		_tileSheet.drawTiles( _shape.graphics , drawData , true , Tilesheet.TILE_SCALE );
		
		
		////////////////////////////////////////////////////////////////////////
		tileSheet.tileSheet.drawData  = [];
		tileSheet.tileSheet.drawDataOffset = 0;
	}
	override public function removeFromGame(game:Game):Void 
	{
		super.removeFromGame(game);
		_container.removeChild(_shape);
		_tileSheet.nmeBitmap.dispose();
		_tileSheet = null;
		_shape =  null;
	}	
}