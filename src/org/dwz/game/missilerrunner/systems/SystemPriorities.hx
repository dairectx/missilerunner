package org.dwz.game.missilerrunner.systems;

/**
 * ...
 * @author Dairectx
 */

class SystemPriorities 
{
	
	public static inline var preUpdate : Int = 10;
	public static inline var update : Int = 20;
	public static inline var move : Int = 30;
	public static inline var resolveCollisions : Int = 40;
	public static inline var render : Int = 50;


	public function new() 
	{
		
	}
	
}