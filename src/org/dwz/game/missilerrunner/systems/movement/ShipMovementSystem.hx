package org.dwz.game.missilerrunner.systems.movement;
import nme.Lib;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.core.NodeList;
import org.dwz.ashx.core.System;
import org.dwz.game.missilerrunner.components.GameState;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.nodes.movement.ShipMovementNode;
/**
 * ...
 * @author Dairectx
 */

class ShipMovementSystem extends System
{

	private var _nodes:NodeList;
	private var _gameState:GameState;
	public function new(gameState:GameState) 
	{
		super();
		_gameState = gameState;
	}
	override public function addToGame(game:Game):Void 
	{
		super.addToGame(game);
		_nodes = game.getNodeList( ShipMovementNode );
		
	}
	
	override public function update(time:Float):Void 
	{
		var node:ShipMovementNode;
		var position:Position;
		var motion:Motion;
		node = _nodes.head;
		
		while ( node != null )
		{
			motion = node.motion;
			
			position = node.position;
			
			position.position.x += motion.velocity.x * time;
			position.position.y -= motion.velocity.y * time;
			if ( position.position.x > _gameState.width - 160*_gameState.scale )
			{
				position.position.x = _gameState.width  - 160*_gameState.scale;
			}
			else if ( position.position.x < 0 )
			{
				position.position.x = 0;
			}
			
			if ( position.position.y > _gameState.height - 358*_gameState.scale)
			{
				position.position.y = _gameState.height - 358*_gameState.scale;
			}
			else if ( position.position.y < 0 )
			{
				position.position.y = 0;
			}
			
			node = node.next;
		}
		
		super.update(time);
	}
	override public function removeFromGame(game:Game):Void 
	{
		super.removeFromGame(game);
		_nodes = null;
	}	
}