package org.dwz.game.missilerrunner.systems.movement;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.core.NodeList;
import org.dwz.ashx.core.System;
import org.dwz.game.missilerrunner.components.GameState;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.nodes.movement.AsteroidsMovementNode;

/**
 * ...
 * @author Dairectx
 */

class AsteroidsMovementSystem extends System
{
	private var _nodes:NodeList;
	private var _gameState:GameState;
	private var _game:Game;
	public function new( gameState:GameState ) 
	{
		super();		
		_gameState = gameState;		
	}
	override public function addToGame(game:Game):Void 
	{
		super.addToGame(game);
		_game = game;
		_nodes = game.getNodeList( AsteroidsMovementNode );
	}
	
	override public function update(time:Float):Void 
	{
		super.update(time);
		var asteroid:AsteroidsMovementNode;
		var motion:Motion;
		var position:Position;
		asteroid = _nodes.head;
		while ( asteroid != null )
		{
			motion = asteroid.motion;
			position = asteroid.position;
			
			position.position.x += motion.velocity.x * time;
			position.position.y += motion.velocity.y * time;
			if ( position.position.y > _gameState.height + 500 * Math.random() )
			{
				_game.removeEntity( asteroid.entity );
			}
			//rotation?
			
			asteroid =  asteroid.next;
		}
	}
	override public function removeFromGame(game:Game):Void 
	{
		super.removeFromGame(game);
		_nodes = null;
		_gameState = null;
	}	
}