package org.dwz.game.missilerrunner.systems;
import haxe.xml.Fast;
import nme.Assets;
import nme.display.Shape;
import nme.display.Tilesheet;
import nme.geom.Rectangle;
import org.dwz.ashx.core.Entity;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.core.NodeList;
import org.dwz.ashx.core.System;
import org.dwz.game.missilerrunner.components.GameState;
import org.dwz.game.missilerrunner.components.SpriteSheet;
import org.dwz.game.missilerrunner.components.TileSheet;
import org.dwz.game.missilerrunner.creators.AsteroidCreator;
import org.dwz.game.missilerrunner.creators.ICreator;
import org.dwz.game.missilerrunner.creators.RocketCreator;
import org.dwz.game.missilerrunner.creators.ShipCreator;
import org.dwz.game.missilerrunner.nodes.collision.RocketCollisionNode;
import org.dwz.game.missilerrunner.nodes.collision.ShipCollisionNode;
import org.dwz.game.missilerrunner.nodes.collision.AsteroidsCollisionNode;
/**
 * ...
 * @author Dairectx
 */

class GameManager extends System
{
	private var _creator:ICreator;
	private var _game:Game;
	private var _gameState:GameState;
	
	private var _ship:NodeList;
	private var _rockets:NodeList;
	private var _asteroids:NodeList;
	private var _tileSheet:TileSheet;
	private var _rocketNum:Int;
	public function new( game:Game , gameState:GameState ) 
	{
		super();
		_game = game;
		_gameState = gameState;
		getSpriteSheetAndCreatTileSheet(game);
	}
	
	override public function addToGame(game:Game):Void 
	{
		super.addToGame(game);
		_ship = game.getNodeList( ShipCollisionNode );
		_rockets = game.getNodeList( RocketCollisionNode );
		_asteroids = game.getNodeList( AsteroidsCollisionNode );
		
		_rockets.nodeAdded.add( onRocketAdded );
		_rockets.nodeRemoved.add( onRocketRemoved );
		
		_asteroids.nodeAdded.add( onAsteroidAdded );
		_asteroids.nodeRemoved.add( onAsteroidRemoved );
		
	}
	override public function removeFromGame(game:Game):Void 
	{
		super.removeFromGame(game);
	}
	override public function update(time:Float):Void 
	{
		super.update(time);
		
		if ( _gameState.state == "playing" )
		{
			if ( _gameState.lives > 0 )
			{
				if ( _ship.isEmpty() == true )
				{
					//创建飞船
					_creator = new ShipCreator( _game , _tileSheet );
					_creator.create(null);
				}
				if ( _rockets.isEmpty() == true )
				{
					_creator = new RocketCreator( _game , _gameState , _tileSheet );
				
					for( i in 0...5 ) _creator.create(null);
				}
				if ( _asteroids.isEmpty() == true )
				{
					_creator = new AsteroidCreator( _game , _gameState , _tileSheet );
				
					for( i in 0...5 ) _creator.create(null);
				}
			}
		}
	}
	
	private function onRocketAdded( o:Dynamic ):Void
	{
		_rocketNum++;
	}
	
	private function onRocketRemoved( o:Dynamic ):Void
	{
		_rocketNum--;
		_creator = new RocketCreator( _game , _gameState , _tileSheet );
		_creator.create(null);
	}
	
	private function onAsteroidAdded( o:Dynamic ):Void
	{
		
	}
	
	private function onAsteroidRemoved( o:Dynamic ):Void
	{
		_creator = new AsteroidCreator( _game , _gameState , _tileSheet );
		_creator.create(null);
	}
	private function getSpriteSheetAndCreatTileSheet(game:Game):Void
	{
		var entity:Entity = new Entity();		
		var spriteSheet:SpriteSheet = new SpriteSheet();
		spriteSheet.bitmapData =  Assets.getBitmapData("assets/ss1.png" , true );
		spriteSheet.bitmapDataConfig = Xml.parse(Assets.getText("assets/ss1.xml"));
		entity.add( spriteSheet );		
		game.addEntity( entity );
		
		
		//rendervisitor
		entity = new Entity();
		var tileSheet:TileSheet = new TileSheet();
		tileSheet.tileSheet = new Tilesheet( spriteSheet.bitmapData );
		var xml:Xml = spriteSheet.bitmapDataConfig.firstElement();
		for ( element in xml.elementsNamed("Sprite") )
		{
			tileSheet.tileSheet.addTileRect( new Rectangle( Std.parseFloat(element.get("x")) , 
															Std.parseFloat(element.get("y")) , 
															Std.parseFloat(element.get("width")) , 
															Std.parseFloat(element.get("height")) ) );
			
		}
		
		tileSheet.drawData = new Array<Float>();
		tileSheet.drawDataOffset = 0;
		
		tileSheet.nameToOffsetTileID = new Hash<Int>();
		tileSheet.nameToOffsetTileID.set( "dog" , 0 );
		tileSheet.nameToOffsetTileID.set( "cat0" , 10 );
		tileSheet.nameToOffsetTileID.set( "cat1" , 11 );
		tileSheet.nameToOffsetTileID.set( "meteor0" , 12 );
		tileSheet.nameToOffsetTileID.set( "meteor1" , 13 );
		tileSheet.nameToOffsetTileID.set( "meteor2" , 14 );
		tileSheet.nameToOffsetTileID.set( "cat2" , 15 );
		tileSheet.nameToOffsetTileID.set( "cat3" , 16 );
		tileSheet.nameToOffsetTileID.set( "cat4" , 17 );
		
		
		tileSheet.canva = new Shape();
		entity.add( tileSheet );
		game.addEntity(entity);
		
		_tileSheet = tileSheet;
		
	}
	
}