package org.dwz.game.missilerrunner.systems.motion;
import neash.ui.Acceleration;
import nme.display.DisplayObjectContainer;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.core.NodeList;
import org.dwz.ashx.core.System;
import nme.ui.Accelerometer;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.MotionControls;
import org.dwz.game.missilerrunner.nodes.motion.ShipControlNode;
import org.dwz.utils.Keyboard;
import nme.events.KeyboardEvent;
import nme.events.Event;

/**
 * ...
 * @author Dairectx
 */

class ShipMotionControlSystem extends System
{

	private var acceleration:Acceleration;
	private var _freshKeyboard:Keyboard;
	
	private var _ships:NodeList;
	public function new(container:DisplayObjectContainer) 
	{
		super();
#if !android
		init(container);
#end
	}
	
	private function init(container:DisplayObjectContainer):Void
	{
		_freshKeyboard = new Keyboard();

		container.stage.addEventListener(KeyboardEvent.KEY_DOWN, _freshKeyboard.HandleKeyDown);
		container.stage.addEventListener(KeyboardEvent.KEY_UP, _freshKeyboard.HandleKeyUp);		
		
	}
	
	private function UpdateKeyBoard():Void
	{
		var ship:ShipControlNode;
		var control:MotionControls;
		var motion:Motion;
		_freshKeyboard.Update();
		
		ship = _ships.head;
		while ( ship != null )
		{	
			var x:Float;
			var y:Float;
			x = 0;
			y = 0;
			control = ship.control;
			if (_freshKeyboard.Pressed("LEFT"))
			{
				x = -0.5;
			}
			if (_freshKeyboard.Pressed("RIGHT"))
			{
				x = 0.5;
			}
			if (_freshKeyboard.Pressed("DOWN"))
			{
				y = -0.5;
			}
			if (_freshKeyboard.Pressed("UP"))
			{
				y = 0.5;
			}
			control.accelerate_x = x ;
			control.accelerate_y = y ;			
			motion = ship.motion;
			motion.velocity.x = control.accelerate_x * 300;
			motion.velocity.y = control.accelerate_y * 300;
			ship = ship.next;
		}
	}
	override public function addToGame(game:Game):Void 
	{
		super.addToGame(game);
		_ships = game.getNodeList( ShipControlNode );
	}
	
	override public function update(time:Float):Void 
	{
		super.update(time);
		
		var ship:ShipControlNode;
		var control:MotionControls;
		var motion:Motion;
		
		ship = _ships.head;
		while ( ship != null )
		{
#if android			
			acceleration = Accelerometer.get();
			control = ship.control;
			control.accelerate_x = acceleration.x - control.accelerate_x_base;
			control.accelerate_y = acceleration.y - control.accelerate_y_base;			
			motion = ship.motion;
			motion.velocity.x = control.accelerate_x * 300;
			motion.velocity.y = control.accelerate_y * 300;
#else
			UpdateKeyBoard();
#end
			ship = ship.next;
		}
	}
	override public function removeFromGame(game:Game):Void 
	{
		super.removeFromGame(game);
		_ships = null;
	}
	
}