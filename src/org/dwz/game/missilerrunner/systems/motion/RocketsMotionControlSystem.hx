package org.dwz.game.missilerrunner.systems.motion;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.core.NodeList;
import org.dwz.ashx.core.System;
import org.dwz.game.missilerrunner.components.Motion;
import org.dwz.game.missilerrunner.components.Position;
import org.dwz.game.missilerrunner.nodes.collision.ShipCollisionNode;
import org.dwz.game.missilerrunner.nodes.movement.RocketsMovementNode;
/**
 * ...
 * @author Dairectx
 */

class RocketsMotionControlSystem extends System
{

	private var _ship:NodeList;
	private var _rocket:NodeList;
	public function new() 
	{
		super();		
	}
	override public function addToGame(game:Game):Void 
	{
		super.addToGame(game);
		_ship = game.getNodeList( ShipCollisionNode );//火箭的运动趋势由飞船的方位决定，所以需要引入一个ship
		_rocket = game.getNodeList(RocketsMovementNode);
		
	}
	
	override public function update(time:Float):Void 
	{
		super.update(time);
		
		var shipNode:ShipCollisionNode;
		var shipPosition:Position;
		
		var rocketNode:RocketsMovementNode;
		var rocketMotion:Motion;
		var rocketPosition:Position;
		
		shipNode = _ship.head;
		if ( shipNode != null )
		{
			rocketNode = _rocket.head;
			while ( rocketNode != null )
			{
				shipPosition = shipNode.position;//飞船此刻位置
				rocketMotion = rocketNode.motion;
				rocketPosition = rocketNode.position;
				var distance:Float = Math.abs((shipPosition.position.y  - rocketPosition.position.y));
				distance = distance < 500 ? 500 : distance ;
				if( shipPosition.position.y < rocketPosition.position.y )
					rocketMotion.velocity.x = (-shipPosition.position.x + rocketPosition.position.x) / ( distance / rocketMotion.velocity.y);
				rocketNode = rocketNode.next;
			}
		}
		
	}
	override public function removeFromGame(game:Game):Void 
	{	
		super.removeFromGame(game);
		_ship = null;
		_rocket = null;
	}	
}