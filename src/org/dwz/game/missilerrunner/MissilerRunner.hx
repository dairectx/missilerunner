package org.dwz.game.missilerrunner;
import nme.display.DisplayObjectContainer;
import nme.Lib;
import org.dwz.ashx.core.Game;
import org.dwz.ashx.tick.FrameTickProvider;
import org.dwz.game.missilerrunner.components.GameState;
import org.dwz.game.missilerrunner.systems.collision.CollisionSystem;
import org.dwz.game.missilerrunner.systems.GameManager;
import org.dwz.game.missilerrunner.systems.motion.AsteroidsMotionControlSystem;
import org.dwz.game.missilerrunner.systems.motion.RocketsMotionControlSystem;
import org.dwz.game.missilerrunner.systems.motion.ShipMotionControlSystem;
import org.dwz.game.missilerrunner.systems.movement.AsteroidsMovementSystem;
import org.dwz.game.missilerrunner.systems.movement.RocketsMovementSystem;
import org.dwz.game.missilerrunner.systems.movement.ShipMovementSystem;
import org.dwz.game.missilerrunner.systems.render.RenderSystem;
import org.dwz.game.missilerrunner.systems.SystemPriorities;

/**
 * ...
 * @author Dairectx
 */

class MissilerRunner 
{

	private var container:DisplayObjectContainer;
	private var game:Game;
	private var tickProvider:FrameTickProvider;
	private var gameState:GameState;
	private var width:Float;
	private var height:Float;
	
	public function new(container:DisplayObjectContainer , width:Float , height:Float ) 
	{
		this.container =  container;
		this.width = width;
		this.height = height;
		prepare();
	}
	
	private function prepare():Void
	{
		game = new Game();
		gameState = new GameState();
		
		game.addSystem(new GameManager( game , gameState  ) , SystemPriorities.preUpdate );
		
		game.addSystem( new AsteroidsMotionControlSystem() , SystemPriorities.update );
		game.addSystem( new RocketsMotionControlSystem() , SystemPriorities.update );
		game.addSystem( new ShipMotionControlSystem(container) , SystemPriorities.update );
		
		game.addSystem( new AsteroidsMovementSystem(gameState) , SystemPriorities.move );
		game.addSystem( new RocketsMovementSystem(gameState) , SystemPriorities.move );
		game.addSystem( new ShipMovementSystem(gameState) , SystemPriorities.move );		
		
		game.addSystem( new CollisionSystem() , SystemPriorities.resolveCollisions );
		
		game.addSystem( new RenderSystem(container) , SystemPriorities.render );
		
		tickProvider = new FrameTickProvider( container );
		gameState.width = width;
		gameState.height = height;
		gameState.scale = Lib.current.stage.stageWidth / 960 < Lib.current.stage.stageHeight / 1440 ? Lib.current.stage.stageWidth / 960 : Lib.current.stage.stageHeight / 1440;
		
	}
	
	public function start():Void
	{
		gameState.level = 0;
		gameState.lives = 1;
		
		tickProvider.add( game.update );
		tickProvider.start();
	}
	
}